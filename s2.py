#-*- coding: utf-8 -*-
from simplecrypt import encrypt
import socket

sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sk.bind(('127.0.0.1', 7001))
sk.listen(1)

print('-- Aguardando --')
con, client = sk.accept()

r = con.recv(1024)
print "recebida mensagem para criptografar"
if r:
    con.send(encrypt('qualquersenha', r))

print "enviada para o cliente"
sk.close()
print "-- FIM --"
