import time
import socket

m = raw_input('Mensagem secreta: ')

for n in range(5):
    try:
        sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sk.connect(('127.0.0.1', 7000))
        break
    except Exception:
	sk.close()
        sk = None
        print 'tentativa ' + str(n+1) + ' servidor 1'
        time.sleep(5)

if not sk:
    for n in range(5):
        try:
            sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sk.connect(('127.0.0.1', 7001))
            break
        except Exception:
	    sk.close()
	    sk = None
            print 'tentativa ' + str(n+1) + ' servidor 2'
            time.sleep(5)

if sk:

    sk.send(m)
    print "encriptando no servidor"
    m = sk.recv(1024)
    print "mensagem encriptada recebida"
    sk.close()

    sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sk.connect(('127.0.0.1', 7002))
    sk.send(m)
    print "enviada encriptada para o destino"
    m = sk.recv(1024)
    print 'mensagem original decifrada pelo destino: ' + m
    sk.close()
else:
    print 'Servico indisponivel, tente mais tarde.'
