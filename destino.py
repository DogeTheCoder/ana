from simplecrypt import decrypt
import socket

sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sk.bind(('127.0.0.1', 7002))
sk.listen(1)

print('-- Aguardando --')
con, client = sk.accept()

r = con.recv(1024)
print "recebida mensagem criptografada"
if r:
	m = decrypt('qualquersenha', r)
	print "Mensagem decifrada: " + m
	con.send(m)

print "enviada (decifrada) para o cliente"
sk.close()
print "-- FIM --"
